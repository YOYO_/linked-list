class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next


class LinkList:

    def __init__(self, values=None):
        self.head = None
        self.tail = None
        self.length = 0

        if values:
            self._initiate_with_values(values)

    def _initiate_with_values(self, values):
        self.head = Node(values[0], None)
        self.tail = self.head
        self.length = 1
        iter = self.head
        for data in values[1:]:
            new_node = Node(data, None)
            self.length += 1
            iter.next = new_node
            self.tail = iter.next
            iter = iter.next

    def __str__(self):
        if self.head is None:
            return "<>"
        itr = self.head
        my_list = '[ '
        while itr:
            my_list += str(itr.data) + ' ->'
            itr = itr.next
        my_list += " None ]"
        return my_list

    def __len__(self):
        return self.length

    def __del__(self):
        return "Deleted"

    def __getitem__(self, index):
        itr = self.head
        for c in range(index):
            itr = itr.next
        return itr.data

    def __contains__(self, item):
        itr = self.head
        while itr:
            if itr.data == item:
                return True
            itr = itr.next
        return False

    def insert(self, index, data):
        if index == 0:
            new_node = Node(data, self.head)
            if self.head is None:
                self.tail = new_node
            self.head = new_node
            self.length += 1
            return
        count = 0
        itr = self.head
        while count < index - 1:
            itr = itr.next
            count += 1

        new_node = Node(data, itr.next)
        itr.next = new_node
        self.length += 1
        if itr == self.tail:
            self.tail = new_node
        return

    def pop(self):
        # pops the first element
        tmp = self.head.data
        self.head = self.head.next
        self.length -= 1
        return tmp

    def append(self, data):
        if self.head is None:
            self.head = Node(data, None)
            self.tail = self.head
        self.tail.next = Node(data, None)
        self.tail = self.tail.next
        self.length += 1

    def remove(self, index):
        itr = self.head
        c = 0
        for c in range(index-1):
            itr = itr.next
        tmp = itr.next
        itr.next = tmp.next
        self.length -= 1
        if self.length == c+2:
            self.tail = itr

        return tmp

    def extend(self, values):
        if self.head is None:
            self._initiate_with_values(values)
            return
        itr = self.tail
        for data in values:
            itr.next = Node(data, None)
            itr = itr.next
            self.length += 1
            self.tail = itr
